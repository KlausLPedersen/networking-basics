Virtualization is the act of simulating hardware using software. 

All computers must have 3 things available to them
	1. CPu
	2. Memory (Ram)
	3. Storage
	
On top of these things they will have some way of getting data in and out of the machine, USB for instance. And a NIC (Network interface card), either wireless or a physical ethernet connection, which connects to a network. 
	
With virtualization you can simulate an actual machine, by sharing some of the host's CPU, memory and storage, to then create a functional virtual version.

For instance you can create virtual machines that run applications that are meant for other operating systems. Or you can use it to segment a large system into many smaller systems.

Every virtual machine you create is independent, a software called a hypervisor, which decouples the virtual machine from the host.

Topics:

[Partitioning](https://www.oracle.com/assets/partitioning-070609.pdf)

Partitioning is separating a server into sections where each section is a separate system. 


[Server Visualization](https://www.vmware.com/topics/glossary/content/server-virtualization)

You use server virtualization to divide a server into more unique virtual servers, that can run its own operating system independently. This has multiple advantages, including cheaper costs, and better performance.

[Hypervisor](https://www.vmware.com/topics/glossary/content/hypervisor)

A hypervisor is a software that creates and runs virtual machines, with a hypervisor you gain several benefits. You can easily divvy up resources for dynamic workloads, it’s way more cost effective compared to using several physical machines, and it’s very flexible.















